﻿using System;

namespace SettingsManager
{
    public class BasicSetting<T> : Setting
    {
        public T value;

        public BasicSetting()
        {
        }

        public BasicSetting(T initialValue)
        {
            value = initialValue;
        }

        public override string ToString()
        {
            return value.ToString();
        }

        public override void setFromString(String text)
        {
            try
            {
                value = (T)Convert.ChangeType(text, typeof(T));
            }
            catch
            {
                // Use default value
            }
        }
    }
}
