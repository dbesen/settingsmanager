﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;
using System.Text.RegularExpressions;

namespace SettingsManager
{
    public class KeyBinding : Setting
    {
        private IList<Keys> keys = new List<Keys>();
        public void addKey(Keys key)
        {
            keys.Add(key);
        }

        public override void setFromString(string value)
        {
            if (value == null) return;
            string[] parts = value.Split(',');
            foreach (string part in parts)
            {
                addOneKeyFromString(part);
            }
        }

        private void addOneKeyFromString(string part)
        {
            try
            {
                Keys key = (Keys)Enum.Parse(typeof(Keys), part);
                keys.Add(key);
            }
            catch
            {
                // Invalid key, just don't add it
            }
        }

        public override string ToString()
        {
            return String.Join(", ", keys);
        }

        public IList<Keys> getKeys()
        {
            return keys;
        }

        public bool contains(Keys key)
        {
            return keys.Contains(key);
        }
    }
}
