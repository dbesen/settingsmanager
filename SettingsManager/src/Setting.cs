﻿namespace SettingsManager
{
    public abstract class Setting
    {
        public string shortDescription;
        public string longDescription;

        abstract public void setFromString(string value);
    }
}
