﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;

namespace SettingsManager
{
    public class SettingsManager<T> where T : struct // enum
    {
        private Dictionary<T, Setting> values = new Dictionary<T, Setting>();
        public void setSetting(T key, Setting value)
        {
            values[key] = value;
        }

        /*
         * Used for testing.
         */
        public Dictionary<T, Setting> getAllSettings()
        {
            return values;
        }

        public F getSetting<F>(T key) where F : Setting
        {
            if (!values.ContainsKey(key))
                return null;
            return (F) values[key];
        }

        public Setting getSetting(T key)
        {
            return getSetting<Setting>(key);
        }

        public override string ToString()
        {
            string toReturn = "";
            foreach (KeyValuePair<T, Setting> value in values)
            {
                string encoded = encode(value.Value.ToString());
                toReturn += value.Key + ":" + encoded + "\r\n";
            }
            return toReturn;
        }

        // todo: is it weird that you have to add all the settings before they'll be populated by this?
        // maybe not... if the file format changes, this is more resiliant to that
        public void loadFromString(string values)
        {
            string[] settings = values.Split('\n', '\r');
            foreach(string setting in settings)
            {
                int firstColon = setting.IndexOf(":");
                if (firstColon < 0) continue;
                string key = setting.Substring(0, firstColon);
                string value = setting.Substring(firstColon + 1);
                setSetting(key, decode(value));
            }
        }

        private void setSetting(String key, String value)
        {
            T result;
            if (Enum.TryParse<T>(key, out result))
            {
                if(values.ContainsKey(result))
                    values[result].setFromString(value);
            }
        }

        public void writeFile(string path)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(path);
            file.Write(this);
            file.Close();
        }

        public void readFileIfExists(string path)
        {
            if (!File.Exists(path)) return;
            System.IO.StreamReader myFile = new System.IO.StreamReader(path);
            string data = myFile.ReadToEnd();
            myFile.Close();
            loadFromString(data);
        }

        internal static string encode(string str)
        {
            string toReturn = str;
            toReturn = toReturn.Replace("\\", @"\\");
            toReturn = toReturn.Replace("\n", @"\n");
            toReturn = toReturn.Replace("\r", @"\r");
            return toReturn;
        }

        internal static string decode(string str)
        {
            // This doesn't work for \\r or \r or \\\r, no matter the order
            //string toReturn = str;
            //toReturn = toReturn.Replace(@"\\", "\\");
            //toReturn = toReturn.Replace(@"\n", "\n");
            //toReturn = toReturn.Replace(@"\r", "\r");
            //return toReturn;

            string toReturn = "";
            for (int i = 0; i < str.Length; i++)
            {
                if(str[i] == '\\') {
                    i++;
                    switch(str[i]) {
                        case 'r':
                            toReturn += "\r";
                            break;
                        case 'n':
                            toReturn += "\n";
                            break;
                        case '\\':
                            toReturn += "\\";
                            break;
                    }
                } else {
                    toReturn += str[i];
                }
            }
            return toReturn;
        }
    }
}
