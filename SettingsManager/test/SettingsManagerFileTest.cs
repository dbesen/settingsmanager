﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using NUnit.Framework;
using Microsoft.Xna.Framework.Input;

namespace SettingsManager.test
{
    [TestFixture]
    class SettingsManagerFileTest
    {
        const string path = "C:\\delme-SettingsManager.txt";

        [SetUp]
        public void setUp()
        {
            assertFileNotThere();
        }

        [TearDown]
        public void tearDown()
        {
            deleteFile();
            assertFileNotThere();
        }

        private void assertFileNotThere()
        {
            Assert.IsFalse(File.Exists(path), "Test file exists unexpectedly! (" + path + ")");
        }

        private void deleteFile()
        {
            File.Delete(path);
        }

        [Test]
        public void writeReadFile()
        {
            SettingsManager<Settings> s = new SettingsManager<Settings>();
            BasicSetting<int> setting1 = new BasicSetting<int>();
            setting1.value = 137;
            KeyBinding setting2 = new KeyBinding();
            setting2.addKey(Keys.Home);
            setting2.addKey(Keys.Enter);
            s.setSetting(Settings.MOVE_FORWARD, setting1);
            s.setSetting(Settings.MOVE_BACKWARD, setting2);
            s.writeFile(path);

            SettingsManager<Settings> s2 = new SettingsManager<Settings>();
            s2.setSetting(Settings.MOVE_FORWARD, new BasicSetting<int>());
            s2.setSetting(Settings.MOVE_BACKWARD, new KeyBinding());
            s2.readFileIfExists(path);
            Assert.AreEqual(2, s2.getAllSettings().Count);

            BasicSetting<int> setting3 = s2.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD);
            KeyBinding setting4 = s2.getSetting<KeyBinding>(Settings.MOVE_BACKWARD);

            Assert.AreEqual(137, setting3.value);
            Assert.AreEqual(2, setting4.getKeys().Count);
            Assert.AreEqual(Keys.Home, setting4.getKeys()[0]);
            Assert.AreEqual(Keys.Enter, setting4.getKeys()[1]);
        }

        [Test]
        public void testSettingContainsStrangeCharacters()
        {
            string expectedValue = "  !@#$%^&*()_+// \\, \\\\, \\\\\\, \\\\\\\\, \r, \n, \r\n, \n\r, \\r, \\n, \\r\\n, \\r\n, \\\r, \\\\r, \\\\\r, \\\\\\r, \", \\\", :, ::, ,,,  ";
            SettingsManager<Settings> s = new SettingsManager<Settings>();
            BasicSetting<string> setting1 = new BasicSetting<string>();
            setting1.value = expectedValue;

            s.setSetting(Settings.MOVE_FORWARD, setting1);
            Console.WriteLine(s);
            s.writeFile(path);

            SettingsManager<Settings> s2 = new SettingsManager<Settings>();
            s2.setSetting(Settings.MOVE_FORWARD, new BasicSetting<string>());
            s2.readFileIfExists(path);
            Assert.AreEqual(1, s2.getAllSettings().Count);
            BasicSetting<string> setting2 = s2.getSetting<BasicSetting<string>>(Settings.MOVE_FORWARD);
            Console.WriteLine(expectedValue);
            Console.WriteLine(setting2.value);
            Assert.AreEqual(expectedValue, setting2.value);
        }

        [Test]
        public void testWriteExistingFile()
        {
            SettingsManager<Settings> s = new SettingsManager<Settings>();
            BasicSetting<int> setting1 = new BasicSetting<int>();
            setting1.value = 137;
            KeyBinding setting2 = new KeyBinding();
            setting2.addKey(Keys.Home);
            setting2.addKey(Keys.Enter);
            s.setSetting(Settings.MOVE_FORWARD, setting1);
            s.setSetting(Settings.MOVE_BACKWARD, setting2);
            s.writeFile(path);

            setting1.value = 180;
            setting2 = new KeyBinding();
            setting2.addKey(Keys.M);
            s.setSetting(Settings.MOVE_BACKWARD, setting2);
            s.writeFile(path);

            SettingsManager<Settings> s2 = new SettingsManager<Settings>();
            s2.setSetting(Settings.MOVE_FORWARD, new BasicSetting<int>());
            s2.setSetting(Settings.MOVE_BACKWARD, new KeyBinding());
            s2.readFileIfExists(path);
            Assert.AreEqual(2, s2.getAllSettings().Count);

            BasicSetting<int> setting3 = s2.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD);
            KeyBinding setting4 = s2.getSetting<KeyBinding>(Settings.MOVE_BACKWARD);

            Assert.AreEqual(180, setting3.value);
            Assert.AreEqual(1, setting4.getKeys().Count);
            Assert.AreEqual(Keys.M, setting4.getKeys()[0]);
        }

        [Test]
        public void testReadFileNotThere()
        {
            SettingsManager<Settings> s = new SettingsManager<Settings>();
            BasicSetting<int> setting1 = new BasicSetting<int>();
            setting1.value = 137;
            KeyBinding setting2 = new KeyBinding();
            setting2.addKey(Keys.Home);
            setting2.addKey(Keys.Enter);
            s.setSetting(Settings.MOVE_FORWARD, setting1);
            s.setSetting(Settings.MOVE_BACKWARD, setting2);

            assertFileNotThere();
            s.readFileIfExists(path);

            BasicSetting<int> setting3 = s.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD);
            KeyBinding setting4 = s.getSetting<KeyBinding>(Settings.MOVE_BACKWARD);

            Assert.AreEqual(137, setting3.value);
            Assert.AreEqual(2, setting4.getKeys().Count);
            Assert.AreEqual(Keys.Home, setting4.getKeys()[0]);
            Assert.AreEqual(Keys.Enter, setting4.getKeys()[1]);
        }
    }
}
