﻿using NUnit.Framework;

namespace SettingsManager
{
    [TestFixture]
    class BasicSettingTest
    {
        [Test]
        public void construct()
        {
            BasicSetting<float> b = new BasicSetting<float>();
            Assert.IsNotNull(b);
        }

        [Test]
        public void descriptions()
        {
            BasicSetting<bool> s = new BasicSetting<bool>();
            s.shortDescription = "short description";
            s.longDescription = "long description";

            Assert.AreEqual("short description", s.shortDescription);
            Assert.AreEqual("long description", s.longDescription);
        }

        [Test]
        public void getValue()
        {
            BasicSetting<int> b = new BasicSetting<int>();
            Assert.AreEqual(0, b.value);
            b.value = 3;
            Assert.AreEqual(3, b.value);
        }

        [Test]
        public void toString()
        {
            BasicSetting<int> b = new BasicSetting<int>();
            b.value = 57;
            Assert.AreEqual("57", b.ToString());

            BasicSetting<string> b2 = new BasicSetting<string>();
            b2.value = "asdf";
            Assert.AreEqual("asdf", b2.ToString());
        }

        [Test]
        public void testValueConstructor()
        {
            BasicSetting<int> b = new BasicSetting<int>(15);
            Assert.AreEqual(b.value, 15);
        }
    }
}
