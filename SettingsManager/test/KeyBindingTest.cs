﻿using NUnit.Framework;
using Microsoft.Xna.Framework.Input;

namespace SettingsManager
{
    [TestFixture]
    class KeyBindingTest
    {
        [Test]
        public void testGetKeysNoKeys()
        {
            KeyBinding k = new KeyBinding();
            Assert.AreEqual(0, k.getKeys().Count);
        }

        [Test]
        public void testGetKeysOneKey()
        {
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.F);
            Assert.AreEqual(1, k.getKeys().Count);
            Assert.AreEqual(Keys.F, k.getKeys()[0]);
        }

        [Test]
        public void testGetKeysTwoKeys()
        {
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.G);
            k.addKey(Keys.Escape);
            Assert.AreEqual(2, k.getKeys().Count);
            Assert.AreEqual(Keys.G, k.getKeys()[0]);
            Assert.AreEqual(Keys.Escape, k.getKeys()[1]);
        }

        [Test]
        public void testGetKeysTwoKeysDifferentOrder()
        {
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.Escape);
            k.addKey(Keys.G);
            Assert.AreEqual(2, k.getKeys().Count);
            Assert.AreEqual(Keys.Escape, k.getKeys()[0]);
            Assert.AreEqual(Keys.G, k.getKeys()[1]);
        }

        [Test]
        public void testToStringOneKey()
        {
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.Left);
            Assert.AreEqual("Left", k.ToString());
        }

        [Test]
        public void testToStringTwoKeys()
        {
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.Right);
            k.addKey(Keys.RightShift);
            Assert.AreEqual("Right, RightShift", k.ToString());
        }

        [Test]
        public void testContainsKey()
        {
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.LeftWindows);
            k.addKey(Keys.M);
            Assert.IsTrue(k.contains(Keys.M));
            Assert.IsTrue(k.contains(Keys.LeftWindows));
            Assert.IsFalse(k.contains(Keys.P));
        }

        [Test]
        public void testFromStringNull()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString(null);
            Assert.AreEqual(0, k.getKeys().Count);
        }

        [Test]
        public void testFromStringEmptyString()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString("");
            Assert.AreEqual(0, k.getKeys().Count);
        }

        [Test]
        public void testFromStringOneKey()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString("A");
            Assert.AreEqual(1, k.getKeys().Count);
            Assert.AreEqual(Keys.A, k.getKeys()[0]);
        }

        [Test]
        public void testFromStringOneKeyLongName()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString("Enter");
            Assert.AreEqual(1, k.getKeys().Count);
            Assert.AreEqual(Keys.Enter, k.getKeys()[0]);
        }

        [Test]
        public void testFromStringOneKeyComma()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString("OemComma");
            Assert.AreEqual(1, k.getKeys().Count);
            Assert.AreEqual(Keys.OemComma, k.getKeys()[0]);
        }

        [Test]
        public void testFromStringTwoKeys()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString("A,Enter");
            Assert.AreEqual(2, k.getKeys().Count);
            Assert.AreEqual(Keys.A, k.getKeys()[0]);
            Assert.AreEqual(Keys.Enter, k.getKeys()[1]);
        }

        [Test]
        public void testFromStringTwoKeysMoreSpace()
        {
            KeyBinding k = new KeyBinding();
            k.setFromString("   A  ,   Enter   ");
            Assert.AreEqual(2, k.getKeys().Count);
            Assert.AreEqual(Keys.A, k.getKeys()[0]);
            Assert.AreEqual(Keys.Enter, k.getKeys()[1]);
        }
    }
}
