﻿using System.Collections.Generic;
using NUnit.Framework;
using Microsoft.Xna.Framework.Input;

namespace SettingsManager.test
{
    [TestFixture]
    class SettingsManagerTest
    {
        [Test]
        public void testEmptySetting()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            Setting setting = m.getSetting(Settings.MOVE_FORWARD);
            Assert.IsNull(setting);
        }

        [Test]
        public void addSetting()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> expectedSetting = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, expectedSetting);
            Setting actualSetting = m.getSetting(Settings.MOVE_FORWARD);
            Assert.AreEqual(expectedSetting, actualSetting);
        }

        [Test]
        public void addTwoSettings()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> expectedSetting = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, expectedSetting);
            BasicSetting<int> expectedSetting2 = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_BACKWARD, expectedSetting2);
            Setting actualSetting = m.getSetting(Settings.MOVE_FORWARD);
            Assert.AreEqual(expectedSetting, actualSetting);
            actualSetting = m.getSetting(Settings.MOVE_BACKWARD);
            Assert.AreEqual(expectedSetting2, actualSetting);
        }

        [Test]
        public void addTwoSettingsToSameKey()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<List<int>> setting = new BasicSetting<List<int>>();
            setting.value = new List<int>();
            setting.value.Add(3);
            setting.value.Add(4);
            m.setSetting(Settings.MOVE_FORWARD, setting);

            Setting settings = m.getSetting(Settings.MOVE_FORWARD);
            Assert.AreEqual(setting, settings);
            Assert.AreEqual(2, setting.value.Count);
            Assert.AreEqual(3, setting.value[0]);
            Assert.AreEqual(4, setting.value[1]);
        }

        [Test]
        public void addSettingToTwoKeys()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> expectedSetting = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, expectedSetting);
            m.setSetting(Settings.MOVE_BACKWARD, expectedSetting);

            Setting actualSettings = m.getSetting(Settings.MOVE_FORWARD);
            Assert.AreEqual(expectedSetting, actualSettings);

            actualSettings = m.getSetting(Settings.MOVE_BACKWARD);
            Assert.AreEqual(expectedSetting, actualSettings);
        }

        [Test]
        public void toString()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            setting.value = 3;
            m.setSetting(Settings.MOVE_FORWARD, setting);
            BasicSetting<int> setting2 = new BasicSetting<int>();
            setting2.value = 4;
            m.setSetting(Settings.MOVE_BACKWARD, setting2);
            Assert.AreEqual("MOVE_FORWARD:3\r\nMOVE_BACKWARD:4\r\n", m.ToString());
        }

        [Test]
        public void fromStringNoSettings()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            m.loadFromString("MOVE_FORWARD: 3\nMOVE_BACKWARD: 4\n");
            Assert.AreEqual(0, m.getAllSettings().Count);
        }

        [Test]
        public void fromStringOneSetting()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            setting.value = 97;
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.loadFromString("MOVE_FORWARD: 3\n");
            Assert.AreEqual(1, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
        }

        [Test]
        public void fromStringOneSettingExtraInfo()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            setting.value = 97;
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.loadFromString("   MOVE_FORWARD  :  3  \n   MOVE_BACKWARD: 4  \n ");
            Assert.AreEqual(1, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
        }

        [Test]
        public void fromStringOneSettingNoEOL()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            setting.value = 97;
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.loadFromString("MOVE_FORWARD: 3");
            Assert.AreEqual(1, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
        }

        [Test]
        public void fromStringInvalidData()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.loadFromString("MOVE_FORWARD: A\nasdf\n\n:::\n:\n4\nMOVEFORWARD: 3\nMOVE_FORWARD: \nMOVE_FORWARD\nMOVE_FORWARD:\n\nMOVE_FORWARD: 3A\nMOVE_FORWARD: 3 A\n\n:\n : \n");
            Assert.AreEqual(1, m.getAllSettings().Count);
            Assert.AreEqual(0, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
        }

        [Test]
        public void fromStringLowercase()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            setting.value = 97;
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.loadFromString("move_forward: 3\nMOVE_BACKWARD: 4\n");
            Assert.AreEqual(1, m.getAllSettings().Count);
            Assert.AreEqual(97, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
        }


        public void fromStringTwoValues()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.loadFromString("MOVE_FORWARD: 3\nMOVE_FORWARD: 4\n");
            Assert.AreEqual(1, m.getAllSettings().Count);
            Assert.AreEqual(4, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
        }

        [Test]
        public void fromStringTwoSettings()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            BasicSetting<int> setting2 = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.setSetting(Settings.MOVE_BACKWARD, setting2);
            m.loadFromString("MOVE_FORWARD: 3\nMOVE_BACKWARD: 4\n");
            Assert.AreEqual(2, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
            Assert.AreEqual(4, m.getSetting<BasicSetting<int>>(Settings.MOVE_BACKWARD).value);
        }

        [Test]
        public void fromStringTwoSettingsCarriageReturn()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            BasicSetting<int> setting2 = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.setSetting(Settings.MOVE_BACKWARD, setting2);
            m.loadFromString("MOVE_FORWARD: 3\r\nMOVE_BACKWARD: 4\r\n");
            Assert.AreEqual(2, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
            Assert.AreEqual(4, m.getSetting<BasicSetting<int>>(Settings.MOVE_BACKWARD).value);
        }

        [Test]
        public void fromStringTwoSettingsCarriageReturnOnly()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            BasicSetting<int> setting2 = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.setSetting(Settings.MOVE_BACKWARD, setting2);
            m.loadFromString("MOVE_FORWARD: 3\rMOVE_BACKWARD: 4\r");
            Assert.AreEqual(2, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
            Assert.AreEqual(4, m.getSetting<BasicSetting<int>>(Settings.MOVE_BACKWARD).value);
        }

        [Test]
        public void fromStringTwoSettingsBackwardsLineEndings()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            BasicSetting<int> setting2 = new BasicSetting<int>();
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.setSetting(Settings.MOVE_BACKWARD, setting2);
            m.loadFromString("MOVE_FORWARD: 3\n\rMOVE_BACKWARD: 4\n\r");
            Assert.AreEqual(2, m.getAllSettings().Count);
            Assert.AreEqual(3, m.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
            Assert.AreEqual(4, m.getSetting<BasicSetting<int>>(Settings.MOVE_BACKWARD).value);
        }

        [Test]
        public void testToFromString()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            BasicSetting<int> setting = new BasicSetting<int>();
            BasicSetting<int> setting2 = new BasicSetting<int>();
            setting.value = 3;
            setting2.value = 4;
            m.setSetting(Settings.MOVE_FORWARD, setting);
            m.setSetting(Settings.MOVE_BACKWARD, setting2);

            string valueString = m.ToString();
            SettingsManager<Settings> r = new SettingsManager<Settings>();
            BasicSetting<int> setting3 = new BasicSetting<int>();
            BasicSetting<int> setting4 = new BasicSetting<int>();
            r.setSetting(Settings.MOVE_FORWARD, setting3);
            r.setSetting(Settings.MOVE_BACKWARD, setting4);
            r.loadFromString(valueString);
            Assert.AreEqual(2, r.getAllSettings().Count);
            Assert.AreEqual(3, r.getSetting<BasicSetting<int>>(Settings.MOVE_FORWARD).value);
            Assert.AreEqual(4, r.getSetting<BasicSetting<int>>(Settings.MOVE_BACKWARD).value);

            string secondValueString = r.ToString();
            Assert.AreEqual(valueString, secondValueString);
        }

        [Test]
        public void testGetKey()
        {
            SettingsManager<Settings> m = new SettingsManager<Settings>();
            KeyBinding k = new KeyBinding();
            k.addKey(Keys.OemSemicolon);
            m.setSetting(Settings.MOVE_FORWARD, k);

            Assert.IsTrue(m.getSetting<KeyBinding>(Settings.MOVE_FORWARD).contains(Keys.OemSemicolon));
        }

        [Test]
        public void testEncode()
        {
            Assert.AreEqual("asdf", SettingsManager<Settings>.encode("asdf"));
            Assert.AreEqual(@"\n", SettingsManager<Settings>.encode("\n"));
            Assert.AreEqual(@"\r", SettingsManager<Settings>.encode("\r"));
            Assert.AreEqual(@"\r\n", SettingsManager<Settings>.encode("\r\n"));
            Assert.AreEqual(@"\n\r", SettingsManager<Settings>.encode("\n\r"));
            Assert.AreEqual(@"\\", SettingsManager<Settings>.encode("\\"));
            Assert.AreEqual(@"\\\\", SettingsManager<Settings>.encode("\\\\"));
            Assert.AreEqual(@"\\\n", SettingsManager<Settings>.encode("\\\n"));
            Assert.AreEqual(@"\\\r", SettingsManager<Settings>.encode("\\\r"));
            Assert.AreEqual(@"\\r", SettingsManager<Settings>.encode("\\r"));
            Assert.AreEqual(@"\\\\r", SettingsManager<Settings>.encode("\\\\r"));
        }

        [Test]
        public void testDecode()
        {
            Assert.AreEqual("asdf", SettingsManager<Settings>.decode("asdf"));
            Assert.AreEqual("\n", SettingsManager<Settings>.decode(@"\n"));
            Assert.AreEqual("\r", SettingsManager<Settings>.decode(@"\r"));
            Assert.AreEqual("\r\n", SettingsManager<Settings>.decode(@"\r\n"));
            Assert.AreEqual("\n\r", SettingsManager<Settings>.decode(@"\n\r"));
            Assert.AreEqual("\\", SettingsManager<Settings>.decode(@"\\"));
            Assert.AreEqual("\\\\", SettingsManager<Settings>.decode(@"\\\\"));
            Assert.AreEqual("\\\n", SettingsManager<Settings>.decode(@"\\\n"));
            Assert.AreEqual("\\\r", SettingsManager<Settings>.decode(@"\\\r"));
            Assert.AreEqual("\\r", SettingsManager<Settings>.decode(@"\\r"));
            Assert.AreEqual("\\\\r", SettingsManager<Settings>.decode(@"\\\\r"));
        }
    }
}
